package com.devcamp.b70.animalapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.b70.animalapi.models.Dog;

@RestController
@RequestMapping("/api")
public class DogController {
    @CrossOrigin
    @GetMapping("/dogs")
    public ArrayList<Dog> getDogs(){
        ArrayList<Dog> arrListDog = new ArrayList<Dog>();

        Dog dog1 = new Dog("Bulldog");
        Dog dog2 = new Dog("Poodle");
        Dog dog3 = new Dog("Corgi");

        arrListDog.add(dog1);
        arrListDog.add(dog2);
        arrListDog.add(dog3);

        return arrListDog;
    }
}
