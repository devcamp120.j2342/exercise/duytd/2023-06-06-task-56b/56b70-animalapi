package com.devcamp.b70.animalapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.b70.animalapi.models.Cat;

@RestController
@RequestMapping("/api")
public class CatController {
    @CrossOrigin
    @GetMapping("/cats")
    public ArrayList<Cat> getCats(){
        ArrayList<Cat> arrListCats = new ArrayList<Cat>();

        Cat cat1 = new Cat("Doremon");
        Cat cat2 = new Cat("Mèo ba tư");
        Cat cat3 = new Cat("Kitty");

        arrListCats.add(cat1);
        arrListCats.add(cat2);
        arrListCats.add(cat3);

        return arrListCats;
    }
}
