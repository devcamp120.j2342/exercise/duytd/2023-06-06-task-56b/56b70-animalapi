package com.devcamp.b70.animalapi.models;

public class Mammal extends Animal{

    public Mammal(String name){
        super(name);
    }

    @Override
    public String toString() {
        return "Mammal [Animal[name = " + getName() + "]]";
    }
}
