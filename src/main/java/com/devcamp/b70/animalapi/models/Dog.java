package com.devcamp.b70.animalapi.models;

public class Dog extends Mammal {
    public Dog(String name){
        super(name);
    }

    public void greets(){
        System.out.println("Woof");
        System.out.println("Wooof");
    }

    public void greets(Dog another){
        System.out.println("Wooof");
    }

    @Override
    public String toString() {
        return "Dog [Mammal[Animal[name=" + getName() + "]]]";
    }

    
}
